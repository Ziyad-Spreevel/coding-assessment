# Coding assessment

## Task
Please create an [.NET Core](#) console app that can be used to import a csv data file that contains product sale transactions [(sample)](https://databasesecuritylogs.blob.core.windows.net/test-files/data.csv). 
The app should analyze the data and display the following metrics:

- Total quantity sold and total revenue per product
- The most popular product based on quantity sold
- Total revenue for all products sold
- The month with the highest revenue

## Submission
- Fork this repository
- Push your code
- Make a pull request

## Notes
- The code should be unit tested whenever possible